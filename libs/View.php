<?php


class Views {
    public function render($controller,$view,$title= ''){
        $controller = get_class($controller);
        $controller = substr($controller, 0, -11);
        $path= './view/'.$controller.'/'.$view;
        
        if(file_exists($path.".php")){
            if($title != ''){
                $this->title= $title;
               /* require "./views/modules/header.php";*/
            }
            elseif(file_exits($path."html")){
              if($title != ''){
                  $this->title = $title;
              }  
              require $path."html";
            } 
        }else{
            echo "ERROR: Vista Invalida".$view."para mostrar";
        }
    }
}
