<?php
    
    require './config.php';

    //var= () ; verdadero :false;
    $url= ( isset($_GET["url"])) ? $_GET["url"] : "Index/index";
    $url = explode("/", $url);
    /* URL LIMPIA como se dividen
   root/controlador/metodo/parametro*/
   // print_r($url);
    
    $controller = ( isset($url[0]) ) ? $url[0] ."_controller" : "Index_controller";
    $method = ( isset($url[1]) && $url[1] != null ) ? $url[1] : "index";
    $params = ( isset($url[2]) && $url[2] != null ) ? $url[2]: null;
    

    
    
    /*echo "Controlador:".$controller;
    echo "<br> Metodo:".$method;
    echo "<br> Parametro:".$params;*/
    
        
    $path ="./controllers/" . $controller .".php";

    //si existe archivo que carga los controladores, 
    // se carga para crear una instancia del contolador
    
if (file_exists($path)){
    
    require $path;
    $controller = new $controller();
         
    //se pregunta si existe una variable llamada metodo, la cual existe poq se difinio arriba
        
          // si existe un metodo dentro del controlador llamado method se deberia hacer la ejecucion del metodo
          // pero antes de eso se pregunta si existe o no existe, sino existe hay un fallo
          // y si existe se pregunta si se ejecuta con o sin parametros
        if (method_exists($controller,$method)){
            
            if (isset($params) && $params != NULL){
            //el index.php usa el controlador y ejecuta el metodo con los parametros
                $controller->{$method}($params);
            }else{
            //sino el index.php usa el controlador y ejecuta el metodo sin parametros
                  $controller->{$method}();
                 }
        }else {
                 exit('Metodo invalido');
              }
    
       
}else{
            exit("Controlador Invalido");
     }

         
    spl_autoload_register(function($class){
        if(file_exists(LIBS.$class.".php")){
            require LIBS.$class.".php";
        }elseif(MODELS.$class.".$php"){
            require MODELS.$class.".$php";
        }else{
            if(file_exists(BS.$class.".php")){
                require BS.$class.".php";
            }else{
                exit('La clase'.$class."no ha sido definida");
            }
        }
    });
     
?>